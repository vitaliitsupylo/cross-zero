
class View:

    def display_output(self, arrData):
        string = '   A   B    C\n' \
                 '     :   :   \n' \
                 '1  ' + arrData[0] + ' : ' + arrData[1] + ' : ' + arrData[2] + ' \n' \
                 '  ___:___:___\n' \
                 '     :   :   \n' \
                 '2  ' + arrData[3] + ' : ' + arrData[4] + ' : ' + arrData[5] + ' \n' \
                 '  ___:___:___\n' \
                 '     :   :   \n' \
                 '3  ' + arrData[6] + ' : ' + arrData[7] + ' : ' + arrData[8] + ' \n' \
                 '     :   :   \n'
        print(string)


