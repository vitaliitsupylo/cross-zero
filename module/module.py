class Module:
    user_account = None
    computer_account = None
    account = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

    def start_game(self):
        value_user = str(input('Выдите чем будите играть: X это 1, O это 2'))

        if value_user == 1:
            self.user_account = 'X'
            self.computer_account = 'O'
        elif value_user == 2:
            self.user_account = 'O'
            self.computer_account = 'X'
        else:
            self.start_game()

        print(self.user_account, self.computer_account)
